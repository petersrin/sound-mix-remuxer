namespace SoundMixRemuxerLibrary.Core
{
    public enum FileType
    {
        Video,
        Audio,
        Folder,
        File
    }
}