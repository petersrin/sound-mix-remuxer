using SoundMixRemuxerLibrary.Core;

namespace SoundMixRemuxerLibrary.Validator
{
    public interface IValidator
    {
        string CleanPath(string path);
        string StripPathToFileName(string path);
        bool FileExtensionIsAccepted(string path, FileType fileType);
        bool PathHasNoExtension(string outExtension);
        bool PathIsValid(string path, FileType fileType);
    }
}