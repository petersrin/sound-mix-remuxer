using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SoundMixRemuxerLibrary.AppConfig;
using SoundMixRemuxerLibrary.Core;
using SoundMixRemuxerLibrary.Exceptions;

namespace SoundMixRemuxerLibrary.Validator
{
    public class WindowsValidator : IValidator
    {
        private readonly IAppConfig _appConfig;
        
        public WindowsValidator(IAppConfig appConfig)
        {
            _appConfig = appConfig;
        }
    
        public string CleanPath(string path)
        {
            if(string.IsNullOrEmpty(path)) { throw new PathIsEmptyException(path);}
        
            var newPath = path;
        
            newPath = StripWhite(newPath);
            newPath = StripQuotes(newPath);
            newPath = StripEndingSlash(newPath);
            return newPath;
        }

        public bool PathIsValid(string path, FileType fileType)
        {
            switch (fileType)
            {
                case FileType.Video:
                case FileType.Audio:
                case FileType.File:
                    return FileExtensionIsAccepted(path, fileType) && PathExists(path, fileType);
                case FileType.Folder:
                    return PathHasNoExtension(path) && PathExists(path, fileType);
                default:
                    throw new ArgumentOutOfRangeException(nameof(fileType), fileType, null);
            }
        }
        
        public bool FileExtensionIsAccepted(string path, FileType fileType)
        {
            switch (fileType)
            {
                case FileType.Video:
                    return IsExtensionValid(path, _appConfig.VideoExtensions);
                case FileType.Audio:
                    return IsExtensionValid(path, _appConfig.AudioExtensions);
                case FileType.File:
                    if (IsExtensionValid(path, _appConfig.FileExtensions)) return true;
                    break;
                default:
                    throw new PathNotAFile(path);
            }

            return false;
        }

        private bool PathExists(string path, FileType fileType)
        {
            switch (fileType)
            {
                case FileType.Video:
                case FileType.Audio:
                case FileType.File:
                    return File.Exists(path);
                case FileType.Folder:
                    if (Directory.Exists(path)) return true;
                    throw new DirectoryNotFoundException();
                default:
                    throw new ArgumentOutOfRangeException(nameof(fileType), fileType, null);
            }
        }

        private static string StripWhite(string path)
        {
            var newPath = path;
            newPath = newPath.Trim();
            return newPath;
        }

        private static string StripQuotes(string path)
        {
            const string quoteCharacter = "\"";
            var newPath = path;
            var lastIndex = newPath.Length - 1;

            if (newPath.EndsWith(quoteCharacter))
            {
                newPath = newPath.Remove(lastIndex);
                lastIndex--;
            }

            if(newPath.StartsWith(quoteCharacter))
            {
                newPath = newPath.Substring(1,lastIndex);
            }

            return newPath;
        }

        private static string StripEndingSlash(string str)
        {
            if (str == null) return null;
            
            if (str.EndsWith("\\") || str.EndsWith("/"))
            {
                str = str.Substring(0, str.Length - 1);
            }

            return str;
        }

        public string StripPathToFileName(string path)
        {
            if (PathHasNoExtension(path)) throw new AmbiguousFileOrDirectoryException();
            var extensionLocation = path.LastIndexOf(".", StringComparison.Ordinal) + 1;
            var extensionLength = path.Length - extensionLocation + 1;
            var lengthWithoutExtension = path.Length - extensionLength;
            var startIndex = path.LastIndexOf("\\", StringComparison.Ordinal) + 1;
            return path.Substring(startIndex, lengthWithoutExtension - startIndex);
        }

        public bool PathHasNoExtension(string path)
        {
            var extensionLocation = path.LastIndexOf(".", StringComparison.Ordinal);
            return extensionLocation == -1;
        }

        private static bool IsExtensionValid(string path, IEnumerable<string> extensions)
        {
            var ext = GetExtension(path);
            var lowerExt = ext.ToLower();
            return ExtensionInList(lowerExt, extensions);
        }

        private static string GetExtension(string path)
        {
            var extensionLocation = path.LastIndexOf(".", StringComparison.Ordinal) + 1;
            if (extensionLocation == 0) throw new MissingExtensionException(path);
        
            var extensionLength = path.Length - extensionLocation;
            return path.Substring(extensionLocation, extensionLength);
        }

        private static bool ExtensionInList(string ext, IEnumerable<string> validExtensions)
        {
            if (validExtensions == null) throw new NoExtensionsDefinedException();
            var extensions = validExtensions.ToList();
            if (extensions.Any(validExtension => ext == validExtension)) return true;
            throw new FileExtensionInvalidException(ext, extensions);
        }
    }
}