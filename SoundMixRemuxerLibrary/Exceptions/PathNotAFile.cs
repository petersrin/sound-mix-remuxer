﻿using System;

namespace SoundMixRemuxerLibrary.Exceptions
{
    [Serializable]
    public class PathNotAFile : ArgumentException
    {
        public string Path { get; }

        public PathNotAFile(string path)
        {
            Path = path;
        }
    }
}