﻿using System;

namespace SoundMixRemuxerLibrary.Exceptions
{
    internal class NoExtensionsDefinedException : Exception
    {
    }
}