﻿using System;

namespace SoundMixRemuxerLibrary.Exceptions
{
    [Serializable]
    public class UnrecognizedOperatingSystemException : Exception
    {
    }
}