﻿using System;

namespace SoundMixRemuxerLibrary.Exceptions
{
    [Serializable]
    public class MissingExtensionException : ArgumentException
    {
        public string Path { get; }

        public MissingExtensionException(string path)
        {
            Path = path;
        }
    }
}