﻿using System;

namespace SoundMixRemuxerLibrary.Exceptions
{
    public class AmbiguousFileOrDirectoryException : ArgumentException
    {
    }
}