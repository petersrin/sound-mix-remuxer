﻿using System;
using System.Collections.Generic;

namespace SoundMixRemuxerLibrary.Exceptions
{
    [Serializable]
    public class FileExtensionInvalidException : ArgumentException
    {
        public string FileExtension { get; }
        public IEnumerable<string> ValidExtensionsForFileType { get; }

        protected FileExtensionInvalidException()
        {
        }

        public FileExtensionInvalidException(string ext, IEnumerable<string> validExtensions) :
            base($"{ext}: File's extension is not contained within accepted extension list.")
        {
            FileExtension = ext;
            ValidExtensionsForFileType = validExtensions;
        }

        public FileExtensionInvalidException(string ext, IEnumerable<string> validExtensions, string message) :
            base(message)
        {
            FileExtension = ext;
            ValidExtensionsForFileType = validExtensions;
        }

        public FileExtensionInvalidException(string ext, IEnumerable<string> validExtensions, string message,
            Exception innerException) :
            base(message, innerException)
        {
            FileExtension = ext;
            ValidExtensionsForFileType = validExtensions;
        }
    }
}