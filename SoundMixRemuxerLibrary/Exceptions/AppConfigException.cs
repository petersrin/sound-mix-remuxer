﻿using System;

namespace SoundMixRemuxerLibrary.Exceptions
{
    [Serializable]
    public class AppConfigException : Exception
    {
    }
}