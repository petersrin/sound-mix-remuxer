﻿using System;

namespace SoundMixRemuxerLibrary.Exceptions
{
    [Serializable]
    public class PathIsEmptyException : ArgumentException
    {
        public string Path { get; }

        public PathIsEmptyException(string path)
        {
            Path = path;
        }
    }
}