using System;
using SoundMixRemuxerLibrary.AppConfig;
using SoundMixRemuxerLibrary.Core;

namespace SoundMixRemuxerLibrary.Command
{
    public class Command : ICommand
    {
        private const string OutExtension = ".mov";
        private readonly IAppConfig _appConfig;
        
        public string InputVideoPath { get; }
        public string InputAudioPath { get; }
        public string OutputFolderPath { get; }
        public string Cmd { get; }

        public Command(string inputVideoPath, string inputAudioPath, string outputFolderPath, IAppConfig appConfig)
        {
            _appConfig= appConfig;
            
            InputVideoPath = _appConfig.Validator.CleanPath(inputVideoPath);
            InputAudioPath = _appConfig.Validator.CleanPath(inputAudioPath);
            OutputFolderPath = _appConfig.Validator.CleanPath(outputFolderPath);

            if (_appConfig.Validator.FileExtensionIsAccepted(InputVideoPath, FileType.Video) &&
                _appConfig.Validator.FileExtensionIsAccepted(InputAudioPath, FileType.Audio) &&
                _appConfig.Validator.PathHasNoExtension(OutputFolderPath))
            {
                SaveToAppConfig();
                Cmd = BuildCommand();
            }
            else throw new ArgumentException();

        }

        private void SaveToAppConfig()
        {
            _appConfig.PreviousVideo = InputVideoPath;
            _appConfig.PreviousAudio = InputAudioPath;
            _appConfig.PreviousOutput = OutputFolderPath;
            
            _appConfig.Save();
        }

        private string BuildCommand()
        {
            var outputFileName = _appConfig.Validator.StripPathToFileName(InputAudioPath) + OutExtension;
            var buildLiteral =
                $@"""""{_appConfig.FfmpegPath}"" -i ""{InputVideoPath}"" -i ""{InputAudioPath}"" -c:v copy -c:a copy -map 0:v:0 -map 1:a:0 ""{OutputFolderPath}\{outputFileName}""";
            return buildLiteral;
        }
    }
}