namespace SoundMixRemuxerLibrary.Command
{
    public interface ICommand
    {
        string InputVideoPath { get; }
        string InputAudioPath { get; }
        string OutputFolderPath { get; }
        string Cmd { get; }
    }
}