﻿using System.Collections.Generic;
using SoundMixRemuxerLibrary.Validator;

namespace SoundMixRemuxerLibrary.AppConfig
{
    public interface IAppConfig
    {
        IValidator Validator { get; }
        List<string> VideoExtensions { get; set; }
        List<string> AudioExtensions { get; set; }
        List<string> FileExtensions { get; set; }
        string PreviousVideo { get; set; }
        string PreviousAudio { get; set; }
        string PreviousOutput { get; set; }
        string FfmpegPath { get; set; }
        void Save();
    }
}