﻿using System;
using System.IO;
using System.Xml.Serialization;
using SoundMixRemuxerLibrary.Exceptions;

namespace SoundMixRemuxerLibrary.AppConfig
{
    public class AppConfigFileIo : IAppConfigFileIo
    {
        private const string AppName = "Sound Mix Remuxer";
        private readonly string _path;
        
        public AppConfigFileIo(string filename)
        {
            var localApplicationData = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
            AppName);

            if (!Directory.Exists(localApplicationData)) Directory.CreateDirectory(localApplicationData);
            
            _path = localApplicationData + "\\" + filename;
        }
        
        public IAppConfig Load()
        {
            var reader = new XmlSerializer(typeof(SerializedAppConfig));

            FileStream file;
            if (!File.Exists(_path))
            {
                file = File.Create(_path);
                file.Close();
            }

            file = File.OpenRead(_path);

            try
            {
                var temp = reader.Deserialize(file) as SerializedAppConfig;
                file.Close();
                if (temp == null) throw new AppConfigException();
                return temp;
            }
            catch (Exception)
            {
                file.Close();
                throw new AppConfigException();
            }
        }

        public void Save(IAppConfig appConfig)
        {
            var serializedAppConfig = PrepareAppConfigForSerialization(appConfig);
            var writer = new XmlSerializer(typeof(SerializedAppConfig));
            Console.WriteLine(_path);
            var file = File.Create(_path);
            writer.Serialize(file, serializedAppConfig);
            file.Close();
        }

        private SerializedAppConfig PrepareAppConfigForSerialization(IAppConfig appConfig)
        {
            var serializableConfig = new SerializedAppConfig();
            serializableConfig.LoadConfig(appConfig);
            return serializableConfig;
        }
    }
}