﻿namespace SoundMixRemuxerLibrary.AppConfig
{
    public interface IAppConfigFileIo
    {
        IAppConfig Load();
        void Save(IAppConfig appConfig);
    }
}