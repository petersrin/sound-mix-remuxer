﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Xml.Serialization;
using SoundMixRemuxerLibrary.Validator;

namespace SoundMixRemuxerLibrary.AppConfig
{
    public abstract class BaseAppConfig : IAppConfig
    {
        public List<string> VideoExtensions { get; set; }
        public List<string> AudioExtensions { get; set; }
        public List<string> FileExtensions { get; set; }
        public string FfmpegPath { get; set; }
        public string PreviousVideo { get; set; }
        public string PreviousAudio { get; set; }
        public string PreviousOutput { get; set; }

        [XmlIgnore] public IValidator Validator { get; protected set; }
        [XmlIgnore] protected IAppConfigFileIo FileIo;
        
        public void LoadConfig(IAppConfig temp)
        {
            VideoExtensions = temp.VideoExtensions;
            AudioExtensions = temp.AudioExtensions;
            FileExtensions = temp.FileExtensions;
            FfmpegPath = temp.FfmpegPath;
            PreviousVideo = temp.PreviousVideo;
            PreviousAudio = temp.PreviousAudio;
            PreviousOutput = temp.PreviousOutput;
        }

        public void Save()
        {
            FileIo.Save(this);
        }

        public bool IsEqualTo(IAppConfig other)
        {
            if (AudioExtensions == null) throw new NullReferenceException();
            if (VideoExtensions == null) throw new NullReferenceException();

            return AudioExtensions.SequenceEqual(other.AudioExtensions) &&
                   VideoExtensions.SequenceEqual(other.VideoExtensions) &&
                   FileExtensions.SequenceEqual(other.FileExtensions) &&
                   FfmpegPath == other.FfmpegPath &&
                   PreviousAudio == other.PreviousAudio &&
                   PreviousVideo == other.PreviousVideo &&
                   PreviousOutput == other.PreviousOutput;
        }
    }
}