using System;
using System.Runtime.InteropServices;
using SoundMixRemuxerLibrary.Exceptions;
using SoundMixRemuxerLibrary.Validator;

namespace SoundMixRemuxerLibrary.AppConfig
{
    public class AppConfig : BaseAppConfig
    {
        public AppConfig(IAppConfigFileIo appConfigFileIo)
        {
            SetValidator();
            FileIo = appConfigFileIo;
            LoadAppConfig();
        }
        
        public void LoadAppConfig()
        {
            try
            {
                var temp = FileIo.Load();
                LoadConfig(temp);
            }
            catch (Exception e)
            {
                if (e is AppConfigException)
                {
                    Console.WriteLine("Missing or invalid config file: loading defaults.");
                    LoadConfig(new DefaultAppConfig(FileIo));
                }
                else throw;
            }
        }

        private void SetValidator()
        { // todo move into DI assembly method
            IValidator validator;

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) validator = new WindowsValidator(this);
            else throw new UnrecognizedOperatingSystemException();

            Validator = validator;
        }
    }
}