﻿using System.Collections.Generic;
using SoundMixRemuxerLibrary.Validator;

namespace SoundMixRemuxerLibrary.AppConfig
{
    public class DefaultAppConfig : BaseAppConfig
    {
        public DefaultAppConfig(IAppConfigFileIo fileIo)
        {
            Validator = new WindowsValidator(this);
            VideoExtensions = new List<string> {"mp4", "h264"};
            AudioExtensions = new List<string> {"wav", "aac"};
            FileExtensions = new List<string> {"exe"};
            FfmpegPath = "ffmpeg.exe";
            PreviousVideo = "";
            PreviousAudio = "";
            PreviousOutput = "";
            FileIo = fileIo;
        }
    }
}