﻿using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using SoundMixRemuxerLibrary.AppConfig;
using SoundMixRemuxerLibrary.Core;
using SoundMixRemuxerLibrary.Exceptions;
using SoundMixRemuxerLibrary.Validator;

namespace SoundMixRemuxerTests
{
    [TestFixture]
    public class WindowsValidatorTests
    {
        private static IAppConfig SetupAppConfig()
        {
            var appConfig = Substitute.For<IAppConfig>();
            appConfig.AudioExtensions = new List<string> {"wav", "aif", "aiff", "flac"};
            appConfig.VideoExtensions = new List<string> {"mov", "mp4", "h264"};

            return appConfig;
        }

        private static IValidator SetupValidator()
        {
            var appConfig = SetupAppConfig();
            return new WindowsValidator(appConfig);
        }
        
        [TestFixture]
        public class CleanPathTests
        {
            private IValidator _validator;

            [SetUp]
            public void Setup()
            {
                _validator = SetupValidator();
            }

            [Test]
            public void PathWithQuotes_CleanPath_PathWithoutQuotes()
            {
                var path = "\"strip these quotes\"";

                var expected = "strip these quotes";

                Assert.AreEqual(expected, _validator.CleanPath(path));
            }

            [Test]
            public void PathWithLeadingWhiteSpace_CleanPath_PathWithoutLeadingWhiteSpace()
            {
                var path = " test";

                var expected = "test";

                Assert.AreEqual(expected, _validator.CleanPath(path));
            }

            [Test]
            public void PathWithTrailingWhiteSpace_CleanPath_PathWithoutTrailingWhiteSpace()
            {
                var path = "test ";

                var expected = "test";

                Assert.AreEqual(expected, _validator.CleanPath(path));
            }

            [Test]
            public void PathWithTrailingSlash_CleanPath_PathWithoutTrailingSlash()
            {
                var path1 = "test\\";
                var path2 = "test/";

                var expected = "test";

                Assert.AreEqual(expected, _validator.CleanPath(path1));
                Assert.AreEqual(expected, _validator.CleanPath(path2));
            }

            [Test]
            public void PathNullOrEmpty_CleanPath_Exception()
            {
                const string path1 = "";
                const string path2 = null;

                Assert.Throws<PathIsEmptyException>(() => _validator.CleanPath(path1));
                Assert.Throws<PathIsEmptyException>(() => _validator.CleanPath(path2));
            }
        }

        [TestFixture]
        public class ExtensionTests
        {
            private IValidator _validator;

            [SetUp]
            public void Setup()
            {
                _validator = SetupValidator();
            }

            [Test]
            public void ValidExtension_TestExtension_ReturnsTrue()
            {
                var path = "test.mov";
                var type = FileType.Video;

                Assert.True(_validator.FileExtensionIsAccepted(path, type));
            }

            [Test]
            public void InvalidExtension_TestExtension_ThrowFileExtensionInvalidException()
            {
                var path = "test.dke";
                var type = FileType.Video;

                Assert.Throws<FileExtensionInvalidException>(() => _validator.FileExtensionIsAccepted(path, type));
            }

            [Test]
            public void Directory_TestExtension_ThrowsPathNotAFile()
            {
                var type = FileType.Folder;

                Assert.Throws<PathNotAFile>(() => _validator.FileExtensionIsAccepted("test", type));
            }

            [Test]
            public void PathWithoutExtension_TestExtension_ThrowsMissingExtension()
            {
                var path = "test";
                var type = FileType.Audio;

                Assert.Throws<MissingExtensionException>(() => _validator.FileExtensionIsAccepted(path, type));
            }
            
            [Test]
            public void HasExtension_DetectsExtension()
            {
                var path = "blah.ext";
                
                Assert.False(_validator.PathHasNoExtension(path));
            }
            
            [Test]
            public void HasNoExtension_DetectsNoExtension()
            {
                var path = "blah";
                
                Assert.True(_validator.PathHasNoExtension(path));
            }
            
        }

        [TestFixture]
        public class Stripper
        {
            private IAppConfig _appConfig;

            [SetUp]
            public void Setup()
            {
                _appConfig = Substitute.For<IAppConfig>();
            }
            
            [Test]
            public void AudioFilePath_StripsToFileNameWithoutExtension()
            {
                var path =
                    @"E:\Current\Diskonnekt\04 Sessions\04 Final Mix\2021-02-17 Memoires v14 Mix\Bounced Files\2021-02-26 Memoires v15 Mix v2.wav";
                var validator = new WindowsValidator(_appConfig);
                var expected = @"2021-02-26 Memoires v15 Mix v2";
                Assert.AreEqual(expected, validator.StripPathToFileName(path));
            }
            
            [Test]
            public void AudioFilePathLongExtension_StripsToFileNameWithoutExtension()
            {
                var path =
                    @"E:\Current\Diskonnekt\04 Sessions\04 Final Mix\2021-02-17 Memoires v14 Mix\Bounced Files\2021-02-26 Memoires v15 Mix v2.aiff";
                var validator = new WindowsValidator(_appConfig);
                var expected = @"2021-02-26 Memoires v15 Mix v2";
                Assert.AreEqual(expected, validator.StripPathToFileName(path));
            }
            
            [Test]
            public void NoExtension_AmbiguousFileDirectoryException()
            {
                var path =
                    @"E:\Current\Diskonnekt\04 Sessions\04 Final Mix\2021-02-17 Memoires v14 Mix\Bounced Files\2021-02-26 Memoires v15 Mix v2";
                var validator = new WindowsValidator(_appConfig);

                Assert.Throws<AmbiguousFileOrDirectoryException>(() => validator.StripPathToFileName(path));
            }
        }
    }
}