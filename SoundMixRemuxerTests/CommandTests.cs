﻿using NSubstitute;
using NUnit.Framework;
using SoundMixRemuxerLibrary.AppConfig;
using SoundMixRemuxerLibrary.Command;
using SoundMixRemuxerLibrary.Exceptions;

namespace SoundMixRemuxerTests
{
    [TestFixture]
    public class CommandTests
    {
        [Test]
        public void CorrectInputs_CorrectOutputs()
        {
            var fileIo = Substitute.For<IAppConfigFileIo>();
            var appConfig = new DefaultAppConfig(fileIo);
                // appConfig.LoadAppConfig();
            
            var command = new Command(
                @"E:\Originals\MEMOIR of a Call Girl_DM_v14_ref_21_02_09.mp4",
                @"E:\2021-02-17 Memoires v14 Mix\Bounced Files\2021-02-26 Memoires v15 Mix v2.wav",
                @"E:\Current\Diskonnekt\03 Video References\Screeners",
                appConfig);
            
            var expected = @"""""ffmpeg.exe"" -i ""E:\Originals\MEMOIR of a Call Girl_DM_v14_ref_21_02_09.mp4"" -i ""E:\2021-02-17 Memoires v14 Mix\Bounced Files\2021-02-26 Memoires v15 Mix v2.wav"" -c:v copy -c:a copy -map 0:v:0 -map 1:a:0 ""E:\Current\Diskonnekt\03 Video References\Screeners\2021-02-26 Memoires v15 Mix v2.mov""";
            
            Assert.AreEqual(expected, command.Cmd);
        }

        [Test]
        public void IncorrectInputV_Excepts()
        {
            var fileIo = Substitute.For<IAppConfigFileIo>();

            Assert.Throws<FileExtensionInvalidException>(() =>
            {
                var appConfig = new DefaultAppConfig(fileIo);
            
                // ReSharper disable once UnusedVariable
                var command = new Command(
                    @"E:\Originals\MEMOIR of a Call Girl_DM_v14_ref_21_02_09.m4a",
                    @"E:\2021-02-17 Memoires v14 Mix\Bounced Files\2021-02-26 Memoires v15 Mix v2.wav",
                    @"E:\Current\Diskonnekt\03 Video References\Screeners",
                    appConfig); 
            });
        }

        [Test]
        public void IncorrectInputA_Excepts()
        {
            var fileIo = Substitute.For<IAppConfigFileIo>();

            Assert.Throws<FileExtensionInvalidException>(() =>
            {
                var appConfig = new DefaultAppConfig(fileIo);
            
                // ReSharper disable once UnusedVariable
                var command = new Command(
                    @"E:\Originals\MEMOIR of a Call Girl_DM_v14_ref_21_02_09.mp4",
                    @"E:\2021-02-17 Memoires v14 Mix\Bounced Files\2021-02-26 Memoires v15 Mix v2.mp3",
                    @"E:\Current\Diskonnekt\03 Video References\Screeners",
                    appConfig); 
            });
        }

        [Test]
        public void IncorrectInputO_Excepts()
        {
            Assert.Throws<PathIsEmptyException>(() =>
            {
                var fileIo = Substitute.For<IAppConfigFileIo>();

                var appConfig = new DefaultAppConfig(fileIo);
            
                // ReSharper disable once UnusedVariable
                var command = new Command(
                    @"E:\Originals\MEMOIR of a Call Girl_DM_v14_ref_21_02_09.mp4",
                    @"E:\2021-02-17 Memoires v14 Mix\Bounced Files\2021-02-26 Memoires v15 Mix v2.wav",
                    @"",
                    appConfig); 
            });
        }
    }
}