﻿using System.Collections.Generic;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using SoundMixRemuxerLibrary.AppConfig;
using SoundMixRemuxerLibrary.Exceptions;

namespace SoundMixRemuxerTests
{
    [TestFixture]
    public class AppConfigTests
    {
        [Test]
        public void LoadingBadFile_LoadsDefaults()
        {
            var fileIo = Substitute.For<IAppConfigFileIo>();
            fileIo.Load().Throws(new AppConfigException());
            var appConfig = new AppConfig(fileIo);

            var defaultAppConfig = new DefaultAppConfig(fileIo);
            
            // ReSharper disable once SuspiciousTypeConversion.Global
            Assert.True(appConfig.IsEqualTo(defaultAppConfig));        }

        [Test]
        public void LoadGoodFile_LoadsFileData()
        {
            var fileIo = Substitute.For<IAppConfigFileIo>();

            var goodAppConfig = Substitute.For<IAppConfig>();
            goodAppConfig.AudioExtensions = new List<string> {"test", "ing"};
            goodAppConfig.VideoExtensions = new List<string> {"test", "ing"};
            goodAppConfig.FileExtensions = new List<string> {"exe"};
            goodAppConfig.FfmpegPath = "test path";
            goodAppConfig.PreviousAudio = "test1";
            goodAppConfig.PreviousVideo = "test2";
            goodAppConfig.PreviousOutput = "test3";
            
            fileIo.Load().Returns(goodAppConfig);
            
            var appConfig = new AppConfig(fileIo);
            appConfig.LoadAppConfig();

            Assert.True(appConfig.IsEqualTo(goodAppConfig));
        }

        [Test]
        public void SaveCall_CallsIntoFileIoOnce()
        {
            var fileIo = Substitute.For<IAppConfigFileIo>();
            var appConfig = new AppConfig(fileIo);
            
            appConfig.Save();

            fileIo.Received().Save(Arg.Any<AppConfig>());
        }

        [Test]
        public void CopyValuesToSerializableObject()
        {
            var appConfig = Substitute.For<IAppConfig>();
            appConfig.AudioExtensions = new List<string> {"test", "ing"};
            appConfig.VideoExtensions = new List<string> {"test", "ing"};
            appConfig.FileExtensions = new List<string> {"exe"};
            appConfig.FfmpegPath = "test path";
            appConfig.PreviousAudio = "test1";
            appConfig.PreviousVideo = "test2";
            appConfig.PreviousOutput = "test3";

            var copy = new SerializedAppConfig();
            
            copy.LoadConfig(appConfig);
            
            Assert.True(copy.IsEqualTo(appConfig));
        }
    }
}