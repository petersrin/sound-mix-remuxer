﻿using System;
using System.Diagnostics;
using SoundMixRemuxerLibrary.AppConfig;
using SoundMixRemuxerLibrary.Command;
using SoundMixRemuxerLibrary.Core;

namespace SoundMixRemuxerConsole
{
    internal static class Program
    {
        private const string AppConfigFilename = "AppConfig.xml";

        private static readonly IAppConfig AppConfig =
            new AppConfig(new AppConfigFileIo(AppConfigFilename));
        
        private static void Main()
        {
            ConfirmFfmpegInstallation();
            var command = GetCommand();
            Console.Write(TryExecute(command));
        }

        private static ICommand GetCommand()
        {
            var inputVideo = MainInputLoop(AppConfig.PreviousVideo, FileType.Video);
            var inputAudio = MainInputLoop(AppConfig.PreviousAudio, FileType.Audio);
            var outputFolder = MainInputLoop(AppConfig.PreviousOutput, FileType.Folder);

            var command = new Command(inputVideo, inputAudio, outputFolder, AppConfig);
            return command;
        }

        private static string MainInputLoop(string previousValue, FileType type)
        {
            var typeString = type.ToString();
            
            Console.WriteLine($"- Input {typeString} Path -");
            
            var usePreviousValue = 
                UsePreviousValuePrompt(previousValue, typeString);

            return GetInputFromUser(previousValue, type, usePreviousValue, typeString);
        }

        private static string UsePreviousValuePrompt(string previousValue, string typeString)
        {
            var usePreviousValue = string.IsNullOrEmpty(previousValue) ? "n" : null;
            while (usePreviousValue == null)
            {
                Console.WriteLine($"Use previous {typeString} value?");
                Console.WriteLine(previousValue);
                Console.Write("(Y/N): ");

                usePreviousValue = Console.ReadLine()?.ToLower();
                if (usePreviousValue == "y" || usePreviousValue == "n") break;
                Console.WriteLine("Input invalid. Please type Y or N");
            }

            return usePreviousValue;
        }

        private static string GetInputFromUser(string previousValue, FileType type, string usePreviousValue, string typeString)
        {
            switch (usePreviousValue)
            {
                case "n":
                    Console.Write($"Input new value for {typeString}: ");
                    return GetPathInput(type);
                case "y":
                    return previousValue;
                default:
                    throw new Exception("Unknown Error");
            }
        }

        private static string GetPathInput(FileType type)
        {
            while (true)
            {
                var pathInput = Console.ReadLine();
                pathInput = AppConfig.Validator.CleanPath(pathInput);
                if(AppConfig.Validator.PathIsValid(pathInput, type)) return pathInput;
            }
        }

        private static string TryExecute(ICommand command)
        {
            var process = CreateProcess(command);

            if (process == null) throw new ArgumentException("Command failed to initialize for unknown reason");

            process.WaitForExit();
            var exitCode = process.ExitCode;
            process.Close();
            
            return exitCode == 0 ? "Completed successfully!" : "Process Failed";
        }

        private static Process CreateProcess(ICommand command)
        {
            var processInfo = new ProcessStartInfo("cmd.exe", "/K " + command.Cmd)
            {
                CreateNoWindow = true,
                UseShellExecute = true,
            };

            return Process.Start(processInfo); 
        }

        private static void ConfirmFfmpegInstallation()
        {
            if (AppConfig.Validator.PathIsValid(AppConfig.FfmpegPath, FileType.File)) return;
            AppConfig.FfmpegPath = GetInputFromUser(AppConfig.FfmpegPath, FileType.File, "n", "FFMPEG Executable");
        }
    }
}