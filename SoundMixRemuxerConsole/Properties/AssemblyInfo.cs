﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SoundMixRemuxerConsole")]
[assembly: AssemblyDescription("Command Line front end to rewrap reference video files with new, associated audio.")]
[assembly: AssemblyConfiguration("DEBUG")]
[assembly: AssemblyCompany("Idyll Sounds")]
[assembly: AssemblyProduct("Sound Mix Remuxer")]
[assembly: AssemblyCopyright("Copyright © Peter Srinivasan  2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("D9D9B31E-6B26-4E28-A9D3-C2D8E09E75E7")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.*")]